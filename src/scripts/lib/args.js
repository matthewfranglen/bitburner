/** @param {NS} ns */

export function parse({ ns, args, script, optional = {}, positional = [] }) {
  const options = {}
  const positions = []
  let name

  for (const argument of args) {
    if (
      name === undefined &&
      argument.constructor === String &&
      argument.startsWith('--')
    ) {
      name = argument.substring(2)
    } else if (name !== undefined) {
      options[name] = argument
      name = undefined
    } else {
      positions.push(argument)
    }
  }
  if (name !== undefined) {
    options[name] = ''
  }

  if (args.length === 0 || 'help' in options) {
    help({
      ns,
      script,
      optional,
      positional,
    })
    return { valid: false, options, positions }
  }
  if (positions.length !== positional.length) {
    help({
      ns,
      script,
      optional,
      positional,
    })
    ns.tprint('ERROR')
    ns.tprint(`\tYou must provide ${positional.length} positional arguments`)
    return { valid: false, options, positions }
  }
  return { valid: true, options, positions }
}

function help({ ns, script, optional, positional }) {
  const options = Object.keys(optional)
  options.sort()

  const introOptions = options.length > 0 ? '[options] ' : ''
  const introPositions = positional.map((argument) => argument[0]).join(' ')

  ns.tprint(`${script} ${introOptions}${introPositions}`)
  if (positional.length > 0) {
    for (const [argument, description] of positional) {
      ns.tprint(`\t${argument}: ${description}`)
    }
  }
  if (options.length > 0) {
    ns.tprint('OPTIONS')
    for (const option of options) {
      const description = optional[option]
      ns.tprint(`\t${option}: ${description}`)
    }
  }
}
