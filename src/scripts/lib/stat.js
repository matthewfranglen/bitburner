/** @param {NS} ns */

export default function stat(ns, host) {
  const hackingLevel = ns.getServerRequiredHackingLevel(host)
  const canHack = ns.getHackingLevel() >= hackingLevel
  const hasRoot = ns.hasRootAccess(host)
  const ports = ns.getServerNumPortsRequired(host)
  const money = ns.getServerMoneyAvailable(host)
  const security = ns.getServerSecurityLevel(host)
  const moneyLimit = ns.getServerMaxMoney(host)
  const securityLimit = ns.getServerMinSecurityLevel(host)
  const ramTotal = ns.getServerMaxRam(host)
  const ramUsed = ns.getServerUsedRam(host)
  const ramAvailable = ramTotal - ramUsed

  return {
    host,
    hackingLevel,
    canHack,
    hasRoot,
    ports,
    money,
    security,
    moneyLimit,
    securityLimit,
    ramTotal,
    ramUsed,
    ramAvailable,
  }
}
