/** @param {NS} ns */

export default function scan(
  ns,
  {
    host,
    depth = undefined,
    isNotPurchased = false,
    isNotHacknet = true,
    isRooted = false,
    canHack = false,
    predicate = undefined,
  }
) {
  const purchasedServers = ns.getPurchasedServers()

  function recurse({ currentHost, currentDepth, parent = undefined }) {
    if (isNotPurchased && purchasedServers.includes(currentHost)) {
      return []
    }
    if (isRooted && !ns.hasRootAccess(currentHost)) {
      return []
    }
    if (isNotHacknet && currentHost.startsWith('hacknet-node-')) {
      return []
    }
    if (
      canHack &&
      ns.getServerRequiredHackingLevel(currentHost) > ns.getHackingLevel()
    ) {
      return []
    }
    if (currentDepth !== undefined && currentDepth <= 0) {
      return [currentHost]
    }
    if (predicate !== undefined && !predicate(currentHost)) {
      return []
    }

    const reachable = ns.scan(currentHost).filter((child) => child !== parent)
    const children = reachable
      .map((child) =>
        recurse({
          currentHost: child,
          currentDepth:
            currentDepth !== undefined ? currentDepth - 1 : undefined,
          parent: currentHost,
        })
      )
      .flat()
    return [...new Set([currentHost, ...children])]
  }

  return recurse({ currentHost: host, currentDepth: depth })
}
