/** @param {NS} ns */

export default async function copy(ns, host) {
  const files = ns.ls(ns.getHostname(), '.js')
  await ns.scp(files, host)
}
