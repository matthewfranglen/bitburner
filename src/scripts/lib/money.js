export function balance(ns) {
  return ns.getServerMoneyAvailable('home')
}

export class Money {
  #ns
  #ratio
  #balance
  #total

  constructor(ns, { ratio }) {
    this.#ns = ns
    this.#ratio = ratio
    this.#balance = 0
    this.#total = balance(ns)
  }

  tick() {
    const total = balance(this.#ns)
    const delta = total - this.#total
    this.#total = total
    if (this.#ratio >= 1) {
      this.#balance = total
    }
    if (delta <= 0) {
      return
    }
    this.#balance += delta * this.#ratio
  }

  has(amount) {
    return this.#balance >= amount && balance(this.#ns) >= amount
  }

  spend(amount) {
    this.#balance -= amount
    this.#total -= amount
  }

  get balance() {
    return this.#balance
  }
}
