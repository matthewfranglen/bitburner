/** @param {NS} ns */
import { parse } from '/scripts/lib/args.js'

export async function main(ns) {
  const { valid, options } = parse({
    ns,
    args: ns.args,
    script: 'clear.js',
    optional: {
      dryRun: 'just find the cost of the server, default true',
    },
    positional: [['noop', 'a blank parameter to confirm start']],
  })
  if (!valid) {
    return
  }

  const { dryRun = true } = options

  ns.ls('home', '.js').forEach((file) => {
    ns.tprint(`delete: ${file}`)
    if (!dryRun) {
      ns.rm(file)
    }
  })
}
