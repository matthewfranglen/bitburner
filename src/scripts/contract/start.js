/** @param {NS} ns */
import { parse } from '/scripts/lib/args.js'
import scan from '/scripts/lib/scan.js'

export async function main(ns) {
  ns.disableLog('ALL')

  const { valid, options } = parse({
    ns,
    args: ns.args,
    script: 'start.js',
    optional: {},
    positional: [['noop', 'a blank parameter to confirm start']],
  })
  if (!valid) {
    return
  }

  const hosts = scan(ns, { host: ns.getHostname() })
  const contracts = hosts
    .map((host) =>
      ns.ls(host, '.cct').map((contract) => ({
        host,
        contract,
      }))
    )
    .reduce((result, value) => [...result, ...value], [])
  ns.tprint(contracts)
}
