/** @param {NS} ns */
import { parse } from '/scripts/lib/args.js'
import { Money } from '/scripts/lib/money.js'

const INCREASE_MONEY_TASK = 'Increase Maximum Money'
const SELL_TASK = 'Sell for Money'
const CORPORATION_MONEY_TASK = 'Sell for Corporation Funds'
const CORPORATION_RESEARCH_TASK = 'Exchange for Corporation Research'
const NOODLES_SERVER = 'n00dles'

export async function main(ns) {
  ;['getServerMoneyAvailable', 'sleep'].forEach(ns.disableLog)

  const { valid, options } = parse({
    ns,
    args: ns.args,
    script: 'hacknet.js',
    optional: {
      ratio:
        'fraction of earned money that can be spent on stocks, default 0.5',
      period:
        'how often to perform actions, in milliseconds, defaults to 10,000',
    },
    positional: [['noop', 'a blank parameter to confirm start']],
  })
  if (!valid) {
    return
  }

  const { period = 10000, ratio = 0.5 } = options
  const hasHacknet = ns.hacknet.maxNumNodes() < Infinity

  if (ratio > 1 || ratio <= 0) {
    ns.tprint(`ERROR: ratio must be (0,1] - you provided ${ratio}`)
    return
  }

  const money = new Money(ns, { ratio })

  while (true) {
    await ns.sleep(period)
    money.tick()

    while (true) {
      const actions = possibleActions(ns, { hasHacknet })
      if (actions.length === 0) {
        ns.tprint('hacknet fully upgraded')
        return
      }

      const action = actions[0]
      ns.print(
        `action: ${action.action} for ${ns.nFormat(
          action.cost,
          '$0.00a'
        )} with benefit ${ns.nFormat(action.benefit, '0,0')}`
      )
      if (!money.has(action.cost)) {
        ns.print(
          `budget: need ${ns.nFormat(action.cost, '$0.00a')} have ${ns.nFormat(
            money.balance,
            '$0.00a'
          )}`
        )
        break
      }

      if (performAction(ns, action)) {
        money.spend(action.cost)
      } else {
        break
      }
      await ns.sleep(10)
    }
    if (hasHacknet) {
      spendHashes(ns)
    }
  }
}

function indexes(ns) {
  return [...new Array(ns.hacknet.numNodes()).keys()]
}

function possibleActions(ns, { hasHacknet }) {
  const stats = stat(ns)
  const actions = [
    ...purchaseNode(ns, { hasHacknet }),
    ...increaseLevel(ns, stats, { hasHacknet }),
    ...increaseRam(ns, stats, { hasHacknet }),
    ...increaseCores(ns, stats, { hasHacknet }),
    ...increaseCache(ns, stats, { hasHacknet }),
  ].filter((action) => Number.isFinite(action.cost))
  actions.sort((a, b) => b.ratio - a.ratio)
  return actions
}

function stat(ns) {
  return indexes(ns).map(ns.hacknet.getNodeStats)
}

function purchaseNode(ns, { hasHacknet }) {
  if (ns.hacknet.numNodes() >= ns.hacknet.maxNumNodes()) {
    return []
  }
  const cost = ns.hacknet.getPurchaseNodeCost()
  const benefit = estimateMoney({ level: 1, ram: 1, cores: 1, hasHacknet })
  return [
    {
      action: 'purchase',
      cost,
      benefit,
      ratio: benefit / cost,
    },
  ]
}

function increaseLevel(ns, stats, { hasHacknet }) {
  return Array.from(stats.entries()).map(([index, node]) => {
    const cost = ns.hacknet.getLevelUpgradeCost(index, 1)
    const benefit = estimateDelta({ ...node, levelDelta: 1, hasHacknet })
    return {
      action: 'increaseLevel',
      cost,
      benefit,
      ratio: benefit / cost,
      index,
    }
  })
}

function increaseRam(ns, stats, { hasHacknet }) {
  return Array.from(stats.entries()).map(([index, node]) => {
    const cost = ns.hacknet.getRamUpgradeCost(index, 1)
    const delta = hasHacknet ? node.ram : 1
    const benefit = estimateDelta({ ...node, ramDelta: delta, hasHacknet })
    return {
      action: 'increaseRam',
      cost,
      benefit,
      ratio: benefit / cost,
      index,
    }
  })
}

function increaseCores(ns, stats, { hasHacknet }) {
  return Array.from(stats.entries()).map(([index, node]) => {
    const cost = ns.hacknet.getCoreUpgradeCost(index, 1)
    const benefit = estimateDelta({ ...node, coresDelta: 1, hasHacknet })
    return {
      action: 'increaseCores',
      cost,
      benefit,
      ratio: benefit / cost,
      index,
    }
  })
}

function increaseCache(ns, stats, { hasHacknet }) {
  if (!hasHacknet) {
    return []
  }
  return Array.from(stats.entries()).map(([index, node]) => {
    const cost = ns.hacknet.getCacheUpgradeCost(index, 1)
    const benefit = 1
    return {
      action: 'increaseCache',
      cost,
      benefit,
      ratio: benefit / cost,
      index,
    }
  })
}

function estimateDelta({
  level,
  levelDelta = 0,
  ram,
  ramDelta = 0,
  cores,
  coresDelta = 0,
  hasHacknet,
}) {
  const estimate = hasHacknet ? estimateHashes : estimateMoney
  const improved = estimate({
    level: level + levelDelta,
    ram: ram + ramDelta,
    cores: cores + coresDelta,
  })
  const base = estimate({ level, ram, cores })
  return improved - base
}

function estimateMoney({ level, ram, cores }) {
  // See https://github.com/danielyxie/bitburner/blob/dev/src/Hacknet/formulas/HacknetNodes.ts#L4
  // simplified as this is only needed to be a ratio
  const ramMultiplier = Math.pow(1.035, ram - 1)
  const coresMultiplier = (cores + 5) / 6
  return level * ramMultiplier * coresMultiplier
}

function estimateHashes({ level, ram, cores }) {
  // See https://github.com/danielyxie/bitburner/blob/dev/src/Hacknet/formulas/HacknetServers.ts#L4
  // simplified as this is only needed to be a ratio
  const ramMultiplier = Math.pow(1.07, Math.log2(ram))
  const coresMultiplier = 1 + (cores - 1) / 5
  return level * ramMultiplier * coresMultiplier
}

function performAction(ns, { action, index = 0 }) {
  if (action === 'purchase') {
    if (ns.hacknet.purchaseNode() === -1) {
      ns.print('purchase: failed')
      return false
    }
    ns.print('purchase: completed')
    return true
  }
  if (action === 'increaseLevel') {
    if (ns.hacknet.upgradeLevel(index, 1)) {
      ns.print(`level: increased level of node ${index}`)
      return true
    }
    ns.print(`level: could not increase level of node ${index}`)
    return false
  }
  if (action === 'increaseRam') {
    if (ns.hacknet.upgradeRam(index, 1)) {
      ns.print(`ram: increased ram of node ${index}`)
      return true
    }
    ns.print(`ram: could not increase ram of node ${index}`)
    return false
  }
  if (action === 'increaseCores') {
    if (ns.hacknet.upgradeCore(index, 1)) {
      ns.print(`cores: increased cores of node ${index}`)
      return true
    }
    ns.print(`cores: could not increase cores of node ${index}`)
    return false
  }
  if (action === 'increaseCache') {
    if (ns.hacknet.upgradeCache(index, 1)) {
      ns.print(`cache: increased cache of node ${index}`)
      return true
    }
    ns.print(`cache: could not increase cache of node ${index}`)
    return false
  }
  ns.tprint(`ERROR: unknown action ${action}`)
  return false
}

function spendHashes(ns) {
  if (hasCorporation(ns)) {
    spendHashesOnCorporation(ns)
  } else {
    spendHashesOnSelf(ns)
  }
}

function hasCorporation(ns) {
  try {
    ns.corporation.getCorporation()
    return true
  } catch (e) {
    return false
  }
}

function spendHashesOnSelf(ns) {
  const hashes = ns.hacknet.numHashes()
  const task = SELL_TASK
  const cost = ns.hacknet.hashCost(task)

  if (hashes < cost) {
    return
  }
  const count = [...new Array(Math.floor(hashes / cost))]
    .map(() => ns.hacknet.spendHashes(task))
    .reduce((result, value) => (value ? result + 1 : result), 0)

  const totalCost = count * cost
  const profit = count * 1000 ** 2
  ns.print(`hash: sold ${totalCost} hashes for $${ns.nFormat(profit, '0.00a')}`)
}

function spendHashesOnCorporation(ns) {
  const hashes = ns.hacknet.numHashes()
  const moneyCost = ns.hacknet.hashCost(CORPORATION_MONEY_TASK)
  const researchCost = ns.hacknet.hashCost(CORPORATION_RESEARCH_TASK)
  const isResearch = moneyCost > researchCost
  const task = isResearch ? CORPORATION_RESEARCH_TASK : CORPORATION_MONEY_TASK
  const cost = Math.min(moneyCost, researchCost)

  if (hashes < cost) {
    return
  }
  const count = [...new Array(Math.floor(hashes / cost))]
    .map(() => ns.hacknet.spendHashes(task))
    .reduce((result, value) => (value ? result + 1 : result), 0)

  const totalCost = ns.nFormat(hashes - ns.hacknet.numHashes(), '0.00a')
  if (isResearch) {
    const research = ns.nFormat(count * 1000, '0.00a')
    ns.print(`hash: sold ${totalCost} hashes for ${research} research`)
  } else {
    const profit = ns.nFormat(count * 1000 ** 3, '0.00a')
    ns.print(`hash: sold ${totalCost} hashes for $${profit} corporate funds`)
  }
}
