/** @param {NS} ns */
import { parse } from '/scripts/lib/args.js'
import scan from '/scripts/lib/scan.js'
import stat from '/scripts/lib/stat.js'
import copy from '/scripts/lib/copy.js'
import { PORT_SERVER_REFRESH, PORT_STOCKMARKET } from '/scripts/lib/ports.js'

const ACTIONS = ['hack', 'grow', 'weaken']

class Hosts {
  #ns
  #hosts
  #refreshPort
  #minimumRam

  constructor(ns, { minimumRam }) {
    const hosts = scan(ns, { host: ns.getHostname() })
    this.#ns = ns
    this.#hosts = hosts
    this.#refreshPort = ns.getPortHandle(PORT_SERVER_REFRESH)
    this.#minimumRam = minimumRam
    ns.print(`Read ${hosts.length} hosts`)
  }

  async copyFiles() {
    await this.#copyFiles(this.#hosts)
  }

  async #copyFiles(hosts) {
    this.#ns.tprint(`Copying files to ${hosts.length} hosts`)
    for (const host of hosts) {
      await copy(this.#ns, host)
      await this.#ns.sleep(10)
    }
  }

  get servers() {
    return this.#hosts
  }

  get workers() {
    const workers = this.#hosts
      .map((host) => stat(this.#ns, host))
      .map(this.#scaleHome)
      .filter((server) => server.hasRoot)
      .filter((server) => server.ramAvailable > this.#minimumRam)
    workers.sort((a, b) => b.ramAvailable - a.ramAvailable)
    return workers
  }

  get targets() {
    const targets = this.#hosts
      .map((host) => stat(this.#ns, host))
      .filter((server) => server.canHack)
      .filter((server) => server.hasRoot)
      .filter((server) => server.moneyLimit > 0)
    return targets
  }

  #scaleHome(server) {
    if (server.host !== 'home') {
      return server
    }
    server.ramAvailable = Math.max(0, server.ramAvailable - server.ramTotal / 2)
    return server
  }

  async refresh() {
    if (this.#refreshPort.empty()) {
      return
    }

    this.#ns.tprint('Refreshing hosts...')
    const hosts = scan(this.#ns, { host: this.#ns.getHostname() })
    this.#refreshPort.clear()

    const newHosts = hosts.filter((host) => !this.#hosts.includes(host))
    this.#hosts = hosts
  }
}

class Tasks {
  #ns
  #securityThreshold
  #ratio

  constructor(ns, { securityThreshold, ratio }) {
    this.#ns = ns
    this.#securityThreshold = securityThreshold
    this.#ratio = ratio
  }

  getTasks({ targets, workers, servers }) {
    const pending = targets.map((target) => this.#getTargetTasks(target)).flat()
    const running = this.#getRunningTasks({ servers })
    const tasks = pending
      .map(({ target, action, threads }) => {
        const key = this.#taskKey({ target, action, threads })
        if (!running.has(key)) {
          return { target, action, threads }
        }
        return { target, action, threads: threads - running.get(key) }
      })
      .filter(({ threads }) => threads > 0)

    tasks.sort((a, b) => ACTIONS.indexOf(a.action) - ACTIONS.indexOf(b.action))
    return tasks
  }

  #getTargetTasks({ host, money, security, moneyLimit, securityLimit }) {
    const tasks = []
    if (security >= securityLimit + this.#securityThreshold) {
      tasks.push({
        target: host,
        action: 'weaken',
        // weaken amount is 0.05 * threads
        // https://bitburner.readthedocs.io/en/latest/netscript/basicfunctions/weaken.html
        threads: Math.ceil((security - securityLimit) / 0.05),
      })
    } else {
      if (security >= securityLimit + 1) {
        tasks.push({
          target: host,
          action: 'weaken',
          // weaken amount is 0.05 * threads
          // https://bitburner.readthedocs.io/en/latest/netscript/basicfunctions/weaken.html
          threads: Math.ceil((security - securityLimit) / 0.05),
        })
      }
      if (money < moneyLimit * this.#ratio) {
        tasks.push({
          target: host,
          action: 'grow',
          // growth rate is difficult to calculate, varies based on how many times it has been called etc
          // https://github.com/danielyxie/bitburner/blob/89aa23f4b31820dcd6328e48716adb0ebbb33c91/src/Server/formulas/grow.ts#L6
          threads: 1,
        })
      }
    }
    if (tasks.length === 0) {
      // hack amount is independent of threads
      // https://github.com/danielyxie/bitburner/blob/89aa23f4b31820dcd6328e48716adb0ebbb33c91/src/NetscriptFunctions/Formulas.ts#L192
      // hack amount is a percentage, so repeating this has reducing returns
      // I tested this in game and it does seem to scale based on threads
      tasks.push({ target: host, action: 'hack', threads: 1 })
    }
    return tasks
  }

  #getRunningTasks({ servers }) {
    function getAction(filename) {
      const parts = filename.split('/')
      return parts[parts.length - 1].split('.')[0]
    }

    return servers
      .map((host) => this.#ns.ps(host))
      .flat()
      .filter(({ filename }) =>
        filename.startsWith('/scripts/hacking/operations/')
      )
      .map(({ filename, threads, args: [target] }) => ({
        target,
        action: getAction(filename),
        threads,
      }))
      .reduce((result, { target, action, threads }) => {
        const key = this.#taskKey({ target, action })
        result.set(key, result.has(key) ? result.get(key) + threads : threads)
        return result
      }, new Map())
  }

  #taskKey({ target, action }) {
    return `${target} :: ${action}`
  }
}

export async function main(ns) {
  ;[
    'exec',
    'getHackingLevel',
    'getServerMaxMoney',
    'getServerMaxRam',
    'getServerMinSecurityLevel',
    'getServerMoneyAvailable',
    'getServerNumPortsRequired',
    'getServerRequiredHackingLevel',
    'getServerSecurityLevel',
    'getServerUsedRam',
    'scan',
    'sleep',
  ].forEach(ns.disableLog)

  const { valid, options } = parse({
    ns,
    args: ns.args,
    script: 'control.js',
    optional: {
      ratio: 'ratio of maximum money to aim for, defaults to 0.75',
      securityThreshold:
        'maximum absolute difference in current security over minimum, defaults to 10',
      period:
        'sleep time in milliseconds between operations, defaults to 10,000ms',
      minimumRam: 'minimum ram for a machine to be used, defaults to 1.75G',
    },
    positional: [['noop', 'a blank parameter to confirm start']],
  })
  if (!valid) {
    return
  }

  const {
    ratio = 0.75,
    securityThreshold = 10,
    period = 10000,
    minimumRam = 1.75,
  } = options

  const hosts = new Hosts(ns, { minimumRam })
  const taskFactory = new Tasks(ns, { securityThreshold, ratio })
  const stockPort = ns.getPortHandle(PORT_STOCKMARKET)
  var stockPosition = stockPort.empty() ? new Map() : stockPort.read()

  // cannot run two instances of the same task with the same arguments
  // this is used to make every invocation unique
  var taskCounter = 0

  await hosts.copyFiles()

  ns.tprint('Starting work')
  while (true) {
    await hosts.refresh()
    if (!stockPort.empty()) {
      while (!stockPort.empty()) {
        stockPosition = stockPort.read()
      }
      ns.print(`stock: updated positions for ${stockPosition.size} servers`)
    }

    const workers = hosts.workers
    if (workers.length === 0) {
      await ns.sleep(period)
      continue
    }

    const tasks = taskFactory.getTasks({
      targets: hosts.targets,
      workers: workers,
      servers: hosts.servers,
    })
    if (tasks.length === 0) {
      await ns.sleep(period)
      continue
    }
    ns.print(`Starting ${tasks.length} tasks using ${workers.length} workers`)

    for (const worker of workers) {
      if (tasks.length === 0) {
        break
      }

      const task = tasks.shift()
      performTask(ns, { ...worker, ...task, taskCounter, stockPosition })
      taskCounter += 1
    }
    await ns.sleep(period)
  }
}

function performTask(
  ns,
  { host, ramAvailable, target, action, threads, taskCounter, stockPosition }
) {
  const script = `/scripts/hacking/operations/${action}.js`
  const ramRequired = ns.getScriptRam(script, host)
  if (ramRequired === 0) {
    ns.tprint(
      `ERROR: cannot exploit ${target} using ${host}, ${script} missing`
    )
    return
  }

  const maxThreads = Math.floor(ramAvailable / ramRequired)
  const taskThreads =
    action === 'weaken' ? Math.min(maxThreads, threads) : maxThreads
  if (threads < 1) {
    ns.tprint(`ERROR: cannot exploit ${target} using ${host}, insufficient ram`)
    return
  }

  const pid = ns.exec(
    script,
    host,
    taskThreads,
    ...args({ target, action, taskCounter, stockPosition })
  )
  if (pid === 0) {
    ns.print(
      `ERROR: could not start ${action} ${target} (host=${host}, threads=${taskThreads})`
    )
  } else {
    ns.print(`${action}: ${target} (host=${host}, threads=${taskThreads})`)
  }
}

function args({ target, action, taskCounter, stockPosition }) {
  if (action === 'weaken') {
    return [target, taskCounter]
  }
  if (!stockPosition.has(target)) {
    return [target, false, taskCounter]
  }
  return [target, action === stockPosition.get(target), taskCounter]
}
