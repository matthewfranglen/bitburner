/** @param {NS} ns */
import { parse } from '/scripts/lib/args.js'
import scan from '/scripts/lib/scan.js'
import stat from '/scripts/lib/stat.js'
import copy from '/scripts/lib/copy.js'

export async function main(ns) {
  const { valid, options, positions } = parse({
    ns,
    args: ns.args,
    script: 'experience.js',
    optional: {
      killAll: 'kill all other scripts on controlled hosts, default true',
    },
    positional: [['target', 'the target to grow, which is joesguns']],
  })
  if (!valid) {
    return
  }

  const { killAll } = options
  const [target] = positions
  if (target !== 'joesguns') {
    ns.tprint('joesguns is the best')
  }

  const hosts = scan(ns, {
    host: ns.getHostname(),
    isRooted: true,
    canHack: false,
  })
  for (const host of hosts) {
    await copy(ns, host)
    if (killAll) {
      await ns.killall(host)
    }
    await ns.sleep(10)
  }

  const servers = hosts
    .map((host) => stat(ns, host))
    .filter(({ ramAvailable }) => ramAvailable >= 1.75)
  ns.tprint('Starting work')
  for (const server of servers) {
    performTask(ns, { target, ...server })
    await ns.sleep(10)
  }
}

function performTask(ns, { target, host, ramAvailable }) {
  const ramRequired = ns.getScriptRam(
    `/scripts/hacking/operations/experience.js`,
    host
  )
  if (ramRequired === 0) {
    ns.tprint(`ERROR: cannot use ${host}, script missing`)
    return
  }

  const threads = Math.floor(ramAvailable / ramRequired)
  if (threads < 1) {
    ns.tprint(`ERROR: cannot use ${host}, insufficient ram`)
    return
  }

  const pid = ns.exec(
    `/scripts/hacking/operations/experience.js`,
    host,
    threads,
    target
  )
  if (pid === 0) {
    ns.print(
      `ERROR: could not start target=${target}, host=${host}, threads=${threads}`
    )
  } else {
    ns.print(`experience: target=${target}, host=${host}, threads=${threads}`)
  }
}
