/** @param {NS} ns */
import { parse } from '/scripts/lib/args.js'

export async function main(ns) {
  ns.disableLog('ALL')

  const { valid, options } = parse({
    ns,
    args: ns.args,
    script: 'start.js',
    optional: {
      period:
        'sleep time in milliseconds between operations, defaults to 1,000ms',
    },
    positional: [['noop', 'a blank parameter to confirm start']],
  })
  if (!valid) {
    return
  }

  const { period = 1000 } = options

  ns.run('/scripts/hacking/unlock.js', 1, '--period', period, 'start')
  ns.run('/scripts/hacking/control.js', 1, '--period', period, 'start')
}
