/** @param {NS} ns */
import { parse } from '/scripts/lib/args.js'
import stat from '/scripts/lib/stat.js'
import copy from '/scripts/lib/copy.js'
import { PORT_SERVER_REFRESH } from '/scripts/lib/ports.js'

export async function main(ns) {
  const { valid, options, positions } = parse({
    ns,
    args: ns.args,
    script: 'unlock.js',
    optional: {
      dryRun: 'just find the cost of the server, default true',
      prefix: 'hostname prefix, default mf-attack',
      destroy: 'destroy a server if necessary, default false',
    },
    positional: [['ram', 'the server ram exponent']],
  })
  if (!valid) {
    return
  }

  const { dryRun = true, prefix = 'mf-attack', destroy = false } = options
  const [ramExponent] = positions
  const ram = 2 ** ramExponent

  const cost = ns.getPurchasedServerCost(ram)
  const money = ns.getServerMoneyAvailable('home')
  if (dryRun || money < cost) {
    ns.tprint(
      `Cost to purchase server with ${ns.nFormat(
        ram,
        '0,0'
      )} GB ram is ${ns.nFormat(cost, '$0.00a')}`
    )
    return
  }

  if (await purchase(ns, { ram, prefix })) {
    return
  }

  const servers = ns.getPurchasedServers()
  if (servers.length < ns.getPurchasedServerLimit()) {
    ns.tprint('Server slots remain, failure to purchase for unknown reason')
    return
  }
  if (!destroy) {
    return
  }

  const smallServers = ns
    .getPurchasedServers()
    .map((host) => stat(ns, host))
    .filter((server) => server.ramTotal < ram)
  if (smallServers.length === 0) {
    ns.tprint(`No server with less than ${ns.nFormat(ram, '0,0')} GB ram`)
    return
  }

  smallServers.sort((a, b) => a.ramTotal - b.ramTotal)
  const smallestServer = smallServers[0]
  ns.tprint(
    `Deleting server with ${ns.nFormat(smallestServer.ramTotal, '0,0')} GB`
  )
  ns.killall(smallestServer.host)
  ns.deleteServer(smallestServer.host)

  await purchase(ns, { ram, prefix })
}

function notify(ns) {
  // write to the port to trigger the control script to refresh hosts
  const handle = ns.getPortHandle(PORT_SERVER_REFRESH)
  handle.write(1)
}

async function purchase(ns, { ram, prefix }) {
  const host = ns.purchaseServer(prefix, ram)
  if (host !== '') {
    ns.tprint(`Purchased ${host} with ${ns.nFormat(ram, '0,0')} GB ram`)
    await copy(ns, host)
    notify(ns)
    return true
  }
  ns.tprint(`Failed to purchase server with ${ns.nFormat(ram, '0,0')} GB ram`)
  return false
}
