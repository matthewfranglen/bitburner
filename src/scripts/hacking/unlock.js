/** @param {NS} ns */
import { parse } from '/scripts/lib/args.js'
import scan from '/scripts/lib/scan.js'
import stat from '/scripts/lib/stat.js'
import { PORT_SERVER_REFRESH } from '/scripts/lib/ports.js'

export async function main(ns) {
  ns.disableLog('ALL')

  const { valid, options } = parse({
    ns,
    args: ns.args,
    script: 'unlock.js',
    optional: {
      period:
        'sleep time in milliseconds between operations, defaults to 10,000ms',
    },
    positional: [['noop', 'a blank parameter to confirm start']],
  })
  if (!valid) {
    return
  }

  const { period = 10000 } = options
  const handle = ns.getPortHandle(PORT_SERVER_REFRESH)

  let lastStats = getStats(ns)
  let servers = getServers(ns, { host: 'home', stats: lastStats })

  while (servers.length > 0) {
    const stats = getStats(ns)
    const unlocked = await unlockAll(ns, { servers, lastStats, stats })
    const useable = hasBecomeUsable(ns, { servers, lastStats, stats })

    if (unlocked || useable) {
      // write to the port to trigger the control script to refresh hosts
      handle.write(1)
      servers = servers
        .map((server) => stat(ns, server.host))
        .filter((server) => needsUnlocking(stats, server))
      if (servers.length > 0) {
        ns.print(`unlock: there are ${servers.length} servers remaining`)
      }
    }

    lastStats = stats

    await ns.sleep(period)
  }
  ns.tprint('all servers have been unlocked')
}

function getStats(ns) {
  return { hackingLevel: ns.getHackingLevel(), ports: getPortCount(ns) }
}

function getPortCount(ns) {
  const files = [
    'brutessh.exe',
    'ftpcrack.exe',
    'relaysmtp.exe',
    'httpworm.exe',
    'sqlinject.exe',
  ]
  let count = 0
  for (const file of files) {
    if (ns.fileExists(file)) {
      count += 1
    }
  }
  return count
}

function getServers(ns, { host, stats }) {
  return scan(ns, { host, isNotPurchased: true })
    .map((host) => stat(ns, host))
    .filter((server) => needsUnlocking(stats, server))
}

function needsUnlocking(stats, { hasRoot, hackingLevel }) {
  return !hasRoot || hackingLevel > stats.hackingLevel
}

async function unlockAll(ns, { servers, lastStats, stats }) {
  const hackable = servers.filter(
    (server) => !server.hasRoot && server.ports <= stats.ports
  )
  for (const server of hackable) {
    if (await unlock(ns, server.host)) {
      ns.print(`hack: ${server.host} now has root`)
    }
  }
  return hackable.length > 0
}

async function unlock(ns, host) {
  if (ns.hasRootAccess(host)) {
    return false
  }

  let portsUnlocked = 0
  if (ns.fileExists('brutessh.exe')) {
    await ns.brutessh(host)
    portsUnlocked += 1
  }
  if (ns.fileExists('ftpcrack.exe')) {
    await ns.ftpcrack(host)
    portsUnlocked += 1
  }
  if (ns.fileExists('relaysmtp.exe')) {
    await ns.relaysmtp(host)
    portsUnlocked += 1
  }
  if (ns.fileExists('httpworm.exe')) {
    await ns.httpworm(host)
    portsUnlocked += 1
  }
  if (ns.fileExists('sqlinject.exe')) {
    await ns.sqlinject(host)
    portsUnlocked += 1
  }
  if (ns.getServerNumPortsRequired(host) <= portsUnlocked) {
    await ns.nuke(host)
  }

  // if (rootAccess && canHack) {
  //   await ns.installBackdoor(host);
  // }

  return ns.hasRootAccess(host)
}

function hasBecomeUsable(ns, { servers, lastStats, stats }) {
  const useable = servers
    .filter((server) => server.hackingLevel > lastStats.hackingLevel)
    .filter((server) => server.hackingLevel <= stats.hackingLevel)

  useable.forEach((server) =>
    ns.print(`exploit: ${server.host} can now be exploited`)
  )

  return useable.length > 0
}
