/** @param {NS} ns */
import { parse } from '/scripts/lib/args.js'

export async function main(ns) {
  const { valid, positions } = parse({
    ns,
    args: ns.args,
    script: 'hack.js',
    positional: [
      ['host', 'the server to grow'],
      ['stock', 'influence the stock market?'],
      ['noop', 'an argument to make all invocations unique'],
    ],
  })
  if (!valid) {
    return
  }

  const [host, stock] = positions

  await ns.hack(host, { stock })
}
