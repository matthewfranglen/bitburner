/** @param {NS} ns */
import { parse } from '/scripts/lib/args.js'

export async function main(ns) {
  const { valid, positions } = parse({
    ns,
    args: ns.args,
    script: 'weaken.js',
    positional: [
      ['host', 'the server to grow'],
      ['noop', 'an argument to make all invocations unique'],
    ],
  })
  if (!valid) {
    return
  }

  const [host] = positions

  await ns.weaken(host)
}
