/** @param {NS} ns */
import { parse } from '/scripts/lib/args.js'

export async function main(ns) {
  const { valid, options, positions } = parse({
    ns,
    args: ns.args,
    script: 'experience.js',
    positional: [['target', 'the target to grow']],
  })
  if (!valid) {
    return
  }

  const [target] = positions

  ns.print('Starting work')
  while (true) {
    await ns.weaken(target)
  }
}
