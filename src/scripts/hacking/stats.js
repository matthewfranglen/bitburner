/** @param {NS} ns */
import { parse } from '/scripts/lib/args.js'
import scan from '/scripts/lib/scan.js'
import stat from '/scripts/lib/stat.js'

export async function main(ns) {
  const { valid, options, positions } = parse({
    ns,
    args: ns.args,
    script: 'stats.js',
    optional: {
      depth:
        'perform recursive traversal from machine to this depth, default 20',
      limit: 'limit output to top N, default 10',
      root: 'require root access, default true',
      hack: 'require hack() and backdoor, default true',
    },
    positional: [['target', 'the machine to stat']],
  })
  if (!valid) {
    return
  }

  const { depth = 20, limit = 10, root = true, hack = true } = options
  const [host] = positions

  const hosts = scan(ns, {
    host,
    depth,
    isRooted: root,
    canHack: hack,
    isNotPurchased: true,
  })
  const stats = hosts.map((host) => stat(ns, host))
  stats.sort((a, b) => b.moneyLimit - a.moneyLimit)

  for (let index = 0; index < stats.length && index < limit; index += 1) {
    show(ns, stats[index])
  }
}

function show(ns, { host, money, security, moneyLimit, securityLimit }) {
  ns.tprint(host)
  ns.tprint(
    `\t money:    ${ns.nFormat(money, '$0.00a')} (maximum ${ns.nFormat(
      moneyLimit,
      '$0.00a'
    )})`
  )
  ns.tprint(
    `\t security: ${ns.nFormat(security, '0,0.00')} (minimum ${ns.nFormat(
      securityLimit,
      '0,0.00'
    )})`
  )
}
