/** @param {NS} ns */
import { parse } from '/scripts/lib/args.js'

export async function main(ns) {
  const { valid, options, positions } = parse({
    ns,
    args: ns.args,
    script: 'find.js',
    positional: [['target', 'the machine to find']],
  })
  if (!valid) {
    return
  }

  const [target] = positions
  const host = ns.getHostname()

  const paths = scan(ns, { host, target })
  for (const path of paths) {
    ns.tprint(path.join(' -> '))
  }
}

function scan(ns, { host, target }) {
  const purchasedServers = ns.getPurchasedServers()

  function recurse({ currentHost, parent = undefined }) {
    if (currentHost === target) {
      return [[currentHost]]
    }
    if (purchasedServers.includes(currentHost)) {
      return []
    }

    const reachable = ns.scan(currentHost).filter((child) => child !== parent)
    return reachable
      .map((child) => recurse({ currentHost: child, parent: currentHost }))
      .flat()
      .filter((child) => child.length > 0)
      .filter((child) => !child.includes(currentHost))
      .map((child) => [currentHost, ...child])
  }

  return recurse({ currentHost: host })
}
