/** @param {NS} ns */
import { parse } from '/scripts/lib/args.js'

export async function main(ns) {
  ns.disableLog('ALL')

  const { valid, options } = parse({
    ns,
    args: ns.args,
    script: 'start.js',
    optional: {
      period:
        'sleep time in milliseconds between operations, defaults to 1,000ms',
    },
    positional: [['noop', 'a blank parameter to confirm start']],
  })
  if (!valid) {
    return
  }

  const { period = 1000, ratio = 0.5 } = options

  while (true) {
    const actions = getActions(ns)
    performActions(ns, actions)
    await ns.sleep(period)
  }
}

const UPGRADE_TYPE = 'UPGRADE'
const ADVERT_TYPE = 'ADVERT'
const EXPAND_TYPE = 'EXPAND'
const HIRE_TYPE = 'HIRE'
const ASSIGN_TYPE = 'ASSIGN'

const UPGRADE_WEIGHT = {
  'Wilson Analytics': 10,
  DreamSense: 5,
  'Smart Factories': 5,
  'Project Insight': 5,
  FocusWires: 2,
  'Smart Storage': 0.1,
}

function getActions(ns) {
  const { name, funds, divisions } = ns.corporation.getCorporation()

  const upgrades = ns.corporation.getUpgradeNames().map((name) => ({
    type: UPGRADE_TYPE,
    name,
    cost: ns.corporation.getUpgradeLevelCost(name),
    weight: UPGRADE_WEIGHT[name] ?? 1,
  }))
  const adverts = divisions
    .map(({ name, awareness, popularity }) => ({
      type: ADVERT_TYPE,
      division: name,
      cost: ns.corporation.getHireAdVertCost(name),
      weight: 10,
      awareness,
      popularity,
    }))
    .filter(
      ({ awareness, popularity }) => Math.max(awareness, popularity) < 1e308
    )

  const offices = divisions
    .flatMap(({ name, cities }) => cities.map((city) => ({ name, city })))
    .map(({ name, city }) => ({
      name,
      city,
      ...ns.corporation.getOffice(name, city),
    }))
  const expansions = offices.map(({ name, city }) => ({
    type: EXPAND_TYPE,
    name,
    city,
    cost: ns.corporation.getOfficeSizeUpgradeCost(name, city, 3),
    weight: name === 'Aevum' ? 10 : 5,
  }))
  const hires = offices
    .filter(({ size, employees }) => size > employees.length)
    .map(({ name, city, size, employees }) => ({
      type: HIRE_TYPE,
      name,
      city,
      count: size - employees.length,
    }))
  const assignments = offices
    .filter(({ employeeJobs: { Unassigned } }) => Unassigned > 0)
    .flatMap(({ name, city, employeeJobs }) => {
      const total = Object.values(employeeJobs).reduce((a, b) => a + b, 0)
      const count = Math.floor(total / 5)
      const remainder = total % 5
      return Object.keys(employeeJobs)
        .filter((role) => !['Training', 'Unassigned'].includes(role))
        .map((role, index) => ({
          type: ASSIGN_TYPE,
          name,
          city,
          role,
          count: index < remainder ? count + 1 : count,
        }))
    })

  return [...upgrades, ...adverts, ...expansions, ...hires, ...assignments]
}

function performActions(ns, actions) {
  const { funds } = ns.corporation.getCorporation()
  const free = actions.filter(({ cost = undefined }) => cost === undefined)
  const expensive = actions
    .filter(({ cost = undefined }) => cost !== undefined)
    .filter(({ cost }) => funds > cost)
    .map((action) => ({
      ...action,
      benefit: (action.weight ?? 1) / action.cost,
    }))
    .sort((a, b) => b.benefit - a.benefit)

  free.forEach((action) => performAction(ns, action))
  expensive.reduce((funds, action) => {
    if (!funds) {
      return false
    }
    return performAction(ns, action)
  }, true)
}

function performAction(ns, action) {
  const { type } = action
  const { funds } = ns.corporation.getCorporation()
  if (type === UPGRADE_TYPE) {
    return performUpgrade(ns, { ...action, funds })
  } else if (type === ADVERT_TYPE) {
    return performAdvert(ns, { ...action, funds })
  } else if (type === EXPAND_TYPE) {
    return performExpand(ns, { ...action, funds })
  } else if (type === HIRE_TYPE) {
    return performHire(ns, { ...action, funds })
  } else if (type === ASSIGN_TYPE) {
    return performAssign(ns, { ...action, funds })
  } else {
    ns.print(`ERROR: unknown action type ${type}: ${JSON.stringify(action)}`)
  }
}

function performUpgrade(ns, { name, cost, funds }) {
  if (funds < cost) {
    return false
  }

  const level = ns.corporation.getUpgradeLevel(name)
  ns.print(`upgrade: ${name} to ${level + 1}`)
  ns.corporation.levelUpgrade(name)
  return true
}

function performAdvert(ns, { division, cost, funds }) {
  if (funds < cost) {
    return false
  }

  const level = ns.corporation.getHireAdVertCount('smokes')
  ns.print(`upgrade: AdVert to ${level + 1}`)
  ns.corporation.hireAdVert(division)
  return true
}

function performExpand(ns, { name, city, cost, funds }) {
  if (funds < cost) {
    return false
  }

  const { size } = ns.corporation.getOffice(name, city)
  ns.print(`expand: ${name} ${city} to ${size + 3}`)
  ns.corporation.upgradeOfficeSize(name, city, 3)
  return true
}

function performHire(ns, { name, city, count }) {
  ;[...new Array(count).keys()].forEach((index) =>
    ns.corporation.hireEmployee(name, city)
  )
  return true
}

function performAssign(ns, { name, city, role, count }) {
  ns.corporation.setAutoJobAssignment(name, city, role, count)
  return true
}
