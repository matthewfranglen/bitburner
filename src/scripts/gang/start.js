/** @param {NS} ns */
import { parse } from '/scripts/lib/args.js'
import { Money } from '/scripts/lib/money.js'

const REDUCE_WANTED_TASK = 'Vigilante Justice'
const TRAIN_COMBAT_TASK = 'Train Combat'
const TERRORISM_TASK = 'Terrorism'
const MUG_TASK = 'Mug People'
const STRONGARM_TASK = 'Strongarm Civilians'
const TRAFFIC_ARMS_TASK = 'Traffick Illegal Arms'
const TRAFFIC_HUMANS_TASK = 'Human Trafficking'
const ASCEND_TASK = 'Ascend'
const DEFEND_TASK = 'Territory Warfare'
const MONEY_TASKS = [STRONGARM_TASK, TRAFFIC_ARMS_TASK, TRAFFIC_HUMANS_TASK]

export async function main(ns) {
  ns.disableLog('ALL')

  const { valid, options } = parse({
    ns,
    args: ns.args,
    script: 'control.js',
    optional: {
      augmentThreshold:
        'maximum spend on a single augment, default 1,000,000,000',
      equipmentThreshold:
        'maximum spend on a single equippable, default 1,000,000',
      ascensionThreshold: '%age of stat increase before ascending, default 20%',
      period:
        'sleep time in milliseconds between operations, defaults to 10,000ms',
      ratio:
        'fraction of earned money that can be spent on augments, default 0.5',
    },
    positional: [['noop', 'a blank parameter to confirm start']],
  })
  if (!valid) {
    return
  }

  const {
    augmentThreshold = 1000000000,
    equipmentThreshold = 1000000,
    ascensionThreshold = 20,
    period = 10000,
    ratio = 0.5,
  } = options
  const ascensionRatio = 1 + ascensionThreshold / 100
  if (ratio >= 1 || ratio <= 0) {
    ns.tprint(`ERROR: ratio must be (0,1) - you provided ${ratio}`)
    return
  }

  await createGang(ns, { period })
  await manageGang(ns, {
    augmentThreshold,
    equipmentThreshold,
    ascensionRatio,
    period,
    ratio,
  })
}

async function createGang(ns, { period }) {
  if (!ns.gang.inGang()) {
    while (ns.heart.break() > -54000) {
      ns.print(`unlock: ${ns.nFormat(ns.heart.break(), '0.00a')} karma`)
      await ns.sleep(period)
    }

    // slum snakes rule!
    if (!ns.gang.createGang('Slum Snakes')) {
      ns.tprint('Cannot create gang, join Slum Snakes first')
      return
    }
    ns.print('unlock: created gang')
  } else {
    ns.tprint('Managing gang')
  }
}

async function manageGang(
  ns,
  { augmentThreshold, equipmentThreshold, ascensionRatio, period, ratio }
) {
  const money = new Money(ns, { ratio })
  while (true) {
    money.tick()

    if (ns.gang.canRecruitMember()) {
      const existing = ns.gang.getMemberNames()
      const recruited = [
        ...new Array(ns.gang.getMemberNames().length + 1).keys(),
      ]
        .map((index) => `member ${index}`)
        .filter((name) => !existing.includes(name))
        .map((name) => ns.gang.recruitMember(name))
        .reduce((result, value) => result || value, false)
      if (!recruited) {
        ns.print('recruit: failed to recruit new member')
      }
    }

    const {
      reduceWanted,
      buildTerritory,
      territoryWarfare,
      territoryWarfareEngaged,
    } = statGang(ns)
    handleWarfare(ns, { territoryWarfare, territoryWarfareEngaged })

    const members = ns.gang
      .getMemberNames()
      .filter((name) => name !== undefined)
      .map((name) => stat(ns, { name, ascensionRatio }))
    members.sort((a, b) => a.combat - b.combat)
    const threshold = Math.floor(members.length / 2)

    const [training, working, defending] = splitMembers(members, {
      buildTerritory,
    })

    handleTrainers(ns, training)
    handleWorkers(ns, working, { reduceWanted })
    defending.forEach(({ name, task }) =>
      doTask(ns, { name, task, newTask: DEFEND_TASK })
    )
    purchaseEquipment(ns, members, {
      augmentThreshold,
      equipmentThreshold,
      money,
    })

    await ns.sleep(period)
  }
}

function statGang(ns) {
  const {
    faction,
    wantedLevel,
    respect,
    wantedLevelGainRate,
    territory,
    territoryWarfareEngaged,
  } = ns.gang.getGangInformation()

  return {
    ...statWanted({ wantedLevel, respect, wantedLevelGainRate }),
    ...statTerritory(ns, { faction, territory }),
    territoryWarfareEngaged,
  }
}

function statWanted({ wantedLevel, respect, wantedLevelGainRate }) {
  if (wantedLevel * 100 < respect) {
    return false
  }
  return { reduceWanted: wantedLevelGainRate > 0 }
}

function statTerritory(ns, { faction, territory }) {
  const memberCount = ns.gang.getMemberNames().length
  const clashChance = Object.keys(ns.gang.getOtherGangInformation())
    .filter((name) => name !== faction)
    .map((name) => ns.gang.getChanceToWinClash(name))
    .reduce((a, b) => Math.min(a, b), 1)
  const buildTerritory = memberCount > 6 && territory < 1
  const territoryWarfare =
    memberCount > 6 && territory < 1 && clashChance > 0.55

  return { buildTerritory, territoryWarfare }
}

function stat(ns, { name, ascensionRatio }) {
  const stats = ns.gang.getMemberInformation(name)
  const ascend = shouldAscend(ns, { ascensionRatio, ...stats })

  return {
    name,
    task: stats.task,
    augmentations: stats.augmentations,
    equipment: stats.equipment,
    ascend,
    combat: combatScore(stats),
  }
}

function shouldAscend(ns, { name, ascensionRatio }) {
  const result = ns.gang.getAscensionResult(name)
  if (result === undefined) {
    return 0
  }
  const { agi, cha, def, dex, hack, str } = result
  return [agi, cha, def, dex, hack, str]
    .map((multiplier) => multiplier > ascensionRatio)
    .reduce((result, value) => value || result, false)
}

function combatScore({ agi, def, dex, str }) {
  return (agi + def + dex + str) / 4
}

function handleWarfare(ns, { territoryWarfare, territoryWarfareEngaged }) {
  if (territoryWarfare == territoryWarfareEngaged) {
    return
  }
  ns.gang.setTerritoryWarfare(territoryWarfare)
}

function splitMembers(members, { buildTerritory }) {
  if (!buildTerritory) {
    const count = Math.floor(members.length / 2)
    return members.reduce(
      (result, member, index) => {
        result[index > count ? 1 : 0].push(member)
        return result
      },
      [[], [], []]
    )
  }
  const count = Math.ceil(members.length / 3)
  return members.reduce(
    (result, member, index) => {
      index = Math.min(2, Math.floor(index / count))
      result[index].push(member)
      return result
    },
    [[], [], []]
  )
}

function handleTrainers(ns, workers) {
  const [ascending, training] = workers.reduce(
    (result, member) => {
      result[member.ascend ? 0 : 1].push(member)
      return result
    },
    [[], []]
  )

  ascending.forEach((member) => doAscend(ns, member))
  training.forEach(({ name, task, combat }) =>
    doTask(ns, {
      name,
      task,
      newTask: trainTask({ combat }),
    })
  )
}

function handleWorkers(ns, workers, { reduceWanted }) {
  function index({ ascend, task }) {
    if (ascend) {
      return 0
    }
    if (task === REDUCE_WANTED_TASK) {
      return 1
    }
    if (MONEY_TASKS.includes(task)) {
      return 2
    }
    return 3
  }

  const [ascending, wanted, money, other] = workers.reduce(
    (result, member) => {
      result[index(member)].push(member)
      return result
    },
    [[], [], [], []]
  )

  ascending.forEach((member) => doAscend(ns, member))
  other.forEach(({ name, combat }) => {
    const task = moneyTask({ combat })
    ns.print(`money: ${name} set to ${task}`)
    ns.gang.setMemberTask(name, task)
  })

  if (reduceWanted) {
    if (money.length === 0) {
      return
    }
    const [{ name, task }] = money
    doTask(ns, { name, task, newTask: REDUCE_WANTED_TASK })
  } else {
    if (wanted.length === 0) {
      return
    }
    const [{ name, task, combat }] = wanted
    doTask(ns, {
      name,
      task,
      newTask: moneyTask({ combat }),
    })
  }
}

function moneyTask({ combat }) {
  if (combat > 1000) {
    return TRAFFIC_HUMANS_TASK
  }
  if (combat > 300) {
    return TRAFFIC_ARMS_TASK
  }
  if (combat > 100) {
    return STRONGARM_TASK
  }
  return MUG_TASK
}

function trainTask({ combat }) {
  if (combat > 300) {
    return TERRORISM_TASK
  }
  return TRAIN_COMBAT_TASK
}

function doTask(ns, { name, task, newTask }) {
  if (task === newTask) {
    return
  }
  ns.print(`task: ${name} doing ${newTask} instead of ${task}`)
  ns.gang.setMemberTask(name, newTask)
}

function doAscend(ns, { name, ascend, task }) {
  if (!ascend) {
    return
  }
  ns.print(`ascend: ${name}`)
  ns.gang.ascendMember(name)
  doTask(ns, { name, task, newTask: TRAIN_COMBAT_TASK })
}

function purchaseEquipment(
  ns,
  members,
  { augmentThreshold, equipmentThreshold, money }
) {
  const purchaseable = ns.gang
    .getEquipmentNames()
    .map((equipmentName) => ({
      equipmentName,
      cost: ns.gang.getEquipmentCost(equipmentName),
      isAugment: ns.gang.getEquipmentType(equipmentName) === 'Augmentation',
    }))
    .filter(({ cost, isAugment }) =>
      cost <= isAugment ? augmentThreshold : equipmentThreshold
    )

  members.forEach(({ name, equipment = [], augmentations = [] }) => {
    purchaseable
      .filter((equipmentName) => !equipment.includes(equipmentName))
      .filter((equipmentName) => !augmentations.includes(equipmentName))
      .forEach(({ equipmentName, cost }) => {
        if (!money.has(cost)) {
          return
        }
        if (ns.gang.purchaseEquipment(name, equipmentName)) {
          ns.print(`purchase: ${name} got ${equipmentName}`)
          money.spend(cost)
        }
      })
  })
}
