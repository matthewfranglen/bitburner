/** @param {NS} ns */
import { parse } from '/scripts/lib/args.js'
import { Money } from '/scripts/lib/money.js'

const ACTIONS = ['hack', 'weaken', 'grow']
var stocks = [
  { symbol: 'AERO', name: 'AeroCorp', server: 'aerocorp' },
  { symbol: 'APHE', name: 'Alpha Enterprises', server: 'alpha-ent' },
  { symbol: 'BLD', name: 'Blade Industries', server: 'blade' },
  { symbol: 'CLRK', name: 'Clarke Incorporated', server: 'clarkinc' },
  { symbol: 'CTK', name: 'CompuTek', server: 'computek' },
  { symbol: 'CTYS', name: 'Catalyst Ventures', server: 'catalyst' },
  { symbol: 'DCOMM', name: 'DefComm', server: 'defcomm' },
  { symbol: 'ECP', name: 'ECorp', server: 'ecorp' },
  { symbol: 'FLCM', name: 'Fulcrum Technologies', server: 'fulcrumassets' },
  { symbol: 'FNS', name: 'FoodNStuff', server: 'foodnstuff' },
  { symbol: 'FSIG', name: 'Four Sigma', server: '4sigma' },
  { symbol: 'GPH', name: 'Global Pharmaceuticals', server: 'global-pharm' },
  { symbol: 'HLS', name: 'Helios Labs', server: 'helios' },
  { symbol: 'ICRS', name: 'Icarus Microsystems', server: 'icarus' },
  { symbol: 'JGN', name: "Joe's Guns", server: 'joesguns' },
  { symbol: 'KGI', name: 'KuaiGong International', server: 'kuai-gong' },
  { symbol: 'LXO', name: 'LexoCorp', server: 'lexo-corp' },
  { symbol: 'MDYN', name: 'Microdyne Technologies', server: 'microdyne' },
  { symbol: 'MGCP', name: 'MegaCorp', server: 'megacorp' },
  { symbol: 'NTLK', name: 'NetLink Technologies', server: 'netlink' },
  { symbol: 'NVMD', name: 'Nova Medical', server: 'nova-med' },
  { symbol: 'OMGA', name: 'Omega Software', server: 'omega-net' },
  { symbol: 'OMN', name: 'Omnia Cybersystems', server: 'omnia' },
  { symbol: 'OMTK', name: 'OmniTek Incorporated', server: 'omnitek' },
  { symbol: 'RHOC', name: 'Rho Contruction', server: 'rho-construction' },
  { symbol: 'SGC', name: 'Sigma Cosmetics', server: 'sigma-cosmetics' },
  { symbol: 'SLRS', name: 'Solaris Space Systems', server: 'solaris' },
  { symbol: 'STM', name: 'Storm Technologies', server: 'stormtech' },
  { symbol: 'SYSC', name: 'SysCore Securities', server: 'syscore' },
  { symbol: 'TITN', name: 'Titan Laboratories', server: 'titan-labs' },
  { symbol: 'UNV', name: 'Universal Energy', server: 'univ-energy' },
  { symbol: 'VITA', name: 'VitaLife', server: 'vitalife' },
  { symbol: 'WDS', name: 'Watchdog Security', server: undefined }, // varies, has watch in the name
]
  .filter((stock) => stock.server !== undefined)
  .reduce((result, stock) => {
    result.set(stock.symbol, stock)
    return result
  }, new Map())
const symbols = new Array(...stocks.keys())

const commission = 100 * 1000

export async function main(ns) {
  ns.disableLog('ALL')

  const { valid, options } = parse({
    ns,
    args: ns.args,
    script: 'control.js',
    optional: {
      ratio:
        'fraction of earned money that can be spent on stocks, default 0.5',
      cycles: 'sleep time in stock market cycles, default 2',
      minimumExpectedReturn:
        'if expected return reaches this or worse then sell the stock immediately, default -0.4',
    },
    positional: [['noop', 'a blank parameter to confirm start']],
  })
  if (!valid) {
    return
  }

  const { ratio = 0.5, cycles = 2, minimumExpectedReturn = -0.4 } = options
  if (minimumExpectedReturn <= -1 || minimumExpectedReturn >= 0) {
    ns.tprint(
      `ERROR: minimumExpectedReturn must be (-1,0) - you provided ${minimumExpectedReturn}`
    )
    return
  }

  while (!(ns.stock.hasTIXAPIAccess() && ns.stock.has4SDataTIXAPI())) {
    ns.print('api: need tix api and 4S data api')
    await ns.sleep(6 * 1000 * cycles + 200)
  }

  await manageStocks(ns, { ratio, cycles, minimumExpectedReturn })
}

async function manageStocks(ns, { ratio, cycles, minimumExpectedReturn }) {
  var stockStatus = getStockStatus(ns, {
    stockStatus: symbols.map((symbol) => ({ symbol })),
  })
  var ownedStocks = stockStatus.filter(({ shares }) => shares > 0)
  const money = new Money(ns, { ratio })

  while (true) {
    stockStatus = getStockStatus(ns, { stockStatus })
    ownedStocks = stockStatus.filter(({ shares }) => shares > 0)
    money.tick()
    status(ns, { money, ownedStocks })

    await sellUnderperformingStock(ns, { ownedStocks, minimumExpectedReturn })
    await buyPromisingStock(ns, { stockStatus, money, cycles })
    await ns.sleep(6 * 1000 * cycles + 200)

    ns.print('---------')
  }
}

function getStockStatus(ns, { stockStatus }) {
  return stockStatus.map(({ symbol, initialReturn = undefined }) => {
    const price = ns.stock.getPrice(symbol)
    const [shares, buyPrice] = ns.stock.getPosition(symbol)
    const volatility = ns.stock.getVolatility(symbol)
    const forecast = ns.stock.getForecast(symbol)
    const expectedReturn = volatility * 2 * (forecast - 0.5)

    if (shares === 0) {
      return {
        symbol,
        price,
        shares,
        buyPrice,
        forecast,
        expectedReturn,
      }
    }

    initialReturn = initialReturn === undefined ? expectedReturn : initialReturn
    const predictedReturn = (expectedReturn - initialReturn) / initialReturn
    return {
      symbol,
      price,
      shares,
      buyPrice,
      forecast,
      expectedReturn,
      initialReturn,
      predictedReturn,
    }
  })
}

async function sellUnderperformingStock(
  ns,
  { ownedStocks, minimumExpectedReturn }
) {
  for (const stock of ownedStocks) {
    if (stock.predictedReturn <= minimumExpectedReturn) {
      await sell(ns, stock)
    } else if (stock.expectedReturn <= 0) {
      await sell(ns, stock)
    }
  }
}

async function sell(ns, { symbol, shares, price, buyPrice }) {
  const profit = getProfit(ns, { symbol, shares, buyPrice })
  await ns.stock.sellStock(symbol, shares)

  const countString = ns.nFormat(shares, '0.00a')
  const profitString = ns.nFormat(Math.abs(profit), '$0.00a')
  const profitSuffix = profit > 0 ? 'in profit' : 'in loss'
  ns.print(
    `sell: ${countString} shares of ${symbol} for ${profitString} ${profitSuffix}`
  )
}

async function buyPromisingStock(ns, { stockStatus, money, cycles }) {
  stockStatus.sort((a, b) => b.expectedReturn - a.expectedReturn)
  for (const stock of stockStatus) {
    const { symbol, shares, expectedReturn, price } = stock
    if (shares > 0) {
      continue
    }
    if (expectedReturn <= 0) {
      continue
    }
    const { count, cost = undefined } = await getShareCount(ns, {
      symbol,
      price,
      money,
    })
    if (count <= 0) {
      continue
    }

    const profit = expectedReturn * cost * cycles
    if (profit > commission) {
      await buy(ns, { ...stock, count, money })

      if (!money.has(10 * 1000 * 1000)) {
        break
      }
    }
  }
}

async function getShareCount(ns, { symbol, price, money }) {
  var count = Math.floor((money.balance - commission) / price)
  while (count > 0) {
    const cost = ns.stock.getPurchaseCost(symbol, count, 'L')
    if (money.has(cost)) {
      return { count, cost }
    }
    const delta = Math.floor((cost - money.balance) / price) + 1
    count -= delta
    await ns.sleep(1)
  }
  return { count: 0 }
}

async function buy(ns, { symbol, price, count, money }) {
  const number = Math.min(ns.stock.getMaxShares(symbol), count)
  const cost = ns.stock.getPurchaseCost(symbol, count, 'L')
  const countString = ns.nFormat(count, '0.00a')
  const costString = ns.nFormat(cost, '$0.00a')

  if ((await ns.stock.buyStock(symbol, number)) === 0) {
    ns.print(
      `ERROR buy failed: ${countString} shares of ${symbol} for ${costString}`
    )
    return
  }
  money.spend(cost)

  ns.print(`buy: ${countString} shares of ${symbol} for ${costString}`)
}

function status(ns, { money, ownedStocks }) {
  if (ownedStocks.length > 0) {
    ns.print(`symbol\t| price \t| profit\t| change`)

    ownedStocks.sort((a, b) => a.symbol.localeCompare(b.symbol))
    ownedStocks.forEach(({ symbol, shares, buyPrice, predictedReturn }) => {
      const profit = getProfit(ns, { symbol, shares, buyPrice })
      const buyPriceString = ns.nFormat(shares * buyPrice, '$0.00a')
      const profitString = ns.nFormat(profit, '$0.00a')
      const changeString = ns.nFormat(predictedReturn, '0.00')
      ns.print(
        `${symbol}\t| ${buyPriceString}\t| ${profitString}\t| ${changeString}`
      )
    })

    const sum = (a, b) => a + b
    const totalPrice = ownedStocks
      .map(({ shares, buyPrice }) => shares * buyPrice)
      .reduce(sum)
    const totalProfit = ownedStocks
      .map((stock) => getProfit(ns, stock))
      .reduce(sum)
    const averageChange =
      ownedStocks.map(({ predictedReturn }) => predictedReturn).reduce(sum) /
      ownedStocks.length

    const totalPriceString = ns.nFormat(totalPrice, '$0.00a')
    const totalProfitString = ns.nFormat(totalProfit, '$0.00a')
    const averageChangeString = ns.nFormat(averageChange, '0.00')
    ns.print(
      `total\t| ${totalPriceString}\t| ${totalProfitString}\t| ${averageChangeString}`
    )
  }

  ns.print(`balance: ${ns.nFormat(money.balance, '$0.00a')}`)
}

function getProfit(ns, { symbol, shares, buyPrice }) {
  const grossProfit = ns.stock.getSaleGain(symbol, shares, 'L')
  return grossProfit - shares * buyPrice - commission
}
