/** @param {NS} ns */
import { parse } from '/scripts/lib/args.js'

export async function main(ns) {
  ns.disableLog('ALL')

  const { valid, options } = parse({
    ns,
    args: ns.args,
    script: 'start.js',
    optional: {
      period:
        'sleep time in milliseconds between operations, defaults to 1,000ms',
      hacking: 'start hacking scripts, default true',
      gang: 'start gang scripts, default true',
      stock: 'start stock market scripts, default true',
      hacknet: 'start hacknet scripts, default true',
      sleeve: 'start sleeve scripts, default true',
      corp: 'start corporation scripts, default true',
    },
    positional: [['noop', 'a blank parameter to confirm start']],
  })
  if (!valid) {
    return
  }

  const {
    period = 1000,
    hacking = true,
    gang = true,
    stock = true,
    hacknet = true,
    sleeve = true,
    corp = true,
  } = options

  if (hacking) {
    ns.run('/scripts/hacking/start.js', 1, '--period', period, 'start')
    await ns.sleep(10)
  }
  if (sleeve) {
    ns.run('/scripts/sleeve/start.js', 1, '--period', period, 'start')
    await ns.sleep(10)
  }
  if (gang) {
    ns.run('/scripts/gang/start.js', 1, '--period', period, 'start')
    await ns.sleep(10)
  }
  if (stock) {
    ns.run('/scripts/stock-market/start.js', 1, 'start')
    await ns.sleep(10)
  }
  if (corp) {
    ns.run('/scripts/corp/start.js', 1, '--period', period, 'start')
    await ns.sleep(10)
  }
  if (hacknet) {
    ns.run('/scripts/hacknet/start.js', 1, 'start')
    await ns.sleep(10)
  }
}
