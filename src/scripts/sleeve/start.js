/** @param {NS} ns */
import { parse } from '/scripts/lib/args.js'
import { Money } from '/scripts/lib/money.js'

const SHOCK_RECOVERY_TASK = 'RECOVERY'
const SYNC_TASK = 'SYNC'
const CRIME_TASK = 'CRIME'
const MUG = 'Mug'
const MURDER = 'Homicide'
const PURCHASE = 'PURCHASE'
const DEBUG = false

export async function main(ns) {
  ns.disableLog('ALL')

  const { valid, options } = parse({
    ns,
    args: ns.args,
    script: 'start.js',
    optional: {
      period:
        'sleep time in milliseconds between operations, defaults to 1,000ms',
      ratio:
        'fraction of earned money that can be spent on augments, default 0.5',
    },
    positional: [['noop', 'a blank parameter to confirm start']],
  })
  if (!valid) {
    return
  }
  if (ns.sleeve.getNumSleeves() === 0) {
    ns.tprint('You have no sleeves')
    return
  }

  const { period = 1000, ratio = 0.5 } = options
  var state = [...new Array(ns.sleeve.getNumSleeves()).keys()].map((index) => ({
    index,
    ...ns.sleeve.getSleeveStats(index),
    ...ns.sleeve.getTask(index),
  }))
  const money = new Money(ns, { ratio })

  while (true) {
    money.tick()
    state = manageSleeves(ns, { state, money })
    await ns.sleep(period)
  }
}

function manageSleeves(ns, { state, money }) {
  const newState = state
    .map((sleeve) => updateSleeve(ns, sleeve))
    .map((sleeve) => [sleeve, sleeveTask(ns, sleeve)])
    .map(([sleeve, task]) => performTask(ns, sleeve, task))
  const purchases = state.flatMap((sleeve) => sleevePurchases(ns, sleeve))
  makePurchases(ns, { money, purchases })
  return newState
}

function updateSleeve(ns, sleeve) {
  const task = ns.sleeve.getTask(sleeve.index)?.type

  return {
    ...sleeve,
    ...ns.sleeve.getSleeveStats(sleeve.index),
    subtype: task === CRIME_TASK ? sleeve.subtype : undefined,
  }
}

function sleeveTask(
  ns,
  {
    index,
    type = undefined,
    subtype = undefined,
    shock,
    sync,
    agility,
    defense,
    dexterity,
    strength,
  }
) {
  const realType = ns.sleeve.getTask(index)?.type

  if (shock > 0) {
    if (realType === SHOCK_RECOVERY_TASK) {
      return []
    }
    return [{ index, type: SHOCK_RECOVERY_TASK }]
  }
  if (sync < 100) {
    if (realType === SYNC_TASK) {
      return []
    }
    return [{ index, type: SYNC_TASK }]
  }

  const combat = agility + defense + dexterity + strength
  const task = {
    index,
    type: CRIME_TASK,
    subtype: combat >= 400 ? MURDER : MUG,
    combat,
  }
  if (realType !== undefined && realType !== CRIME_TASK) {
    return []
  }
  if (task.subtype === subtype) {
    return []
  }
  return [task]
}

function performTask(ns, sleeve, task) {
  if (task.length === 0) {
    return sleeve
  }
  const [{ index, type, subtype = undefined }] = task
  if (type === SHOCK_RECOVERY_TASK) {
    ns.sleeve.setToShockRecovery(index)
    ns.print(`task: set sleeve ${sleeve.index} to shock recovery`)
  } else if (type === SYNC_TASK) {
    ns.sleeve.setToSynchronize(index)
    ns.print(`task: set sleeve ${sleeve.index} to synchronize`)
  } else if (type === CRIME_TASK) {
    ns.sleeve.setToCommitCrime(index, subtype)
    ns.print(`task: set sleeve ${sleeve.index} to CRIME ${subtype}`)
  }
  return { ...sleeve, type, subtype }
}

function sleevePurchases(ns, { index, shock }) {
  if (shock > 0) {
    return []
  }
  const count = ns.sleeve.getSleeveAugmentations(index).length
  const augmentations = ns.sleeve.getSleevePurchasableAugs(index)

  return augmentations.map(({ name, cost }) => ({ index, name, cost, count }))
}

function makePurchases(ns, { money, purchases }) {
  purchases.sort((a, b) => {
    if (a.count === b.count) {
      return a.cost - b.cost
    }
    return a.count - b.count
  })
  if (DEBUG) {
    const budget = ns.nFormat(money.balance, '0.00a')
    const price = ns.nFormat(purchases[0].cost, '0.00a')
    ns.print(`purchase: budget is $${budget} aug cost is $${price}`)
  }
  purchases.reduce((proceed, { index, name, cost }) => {
    if (!proceed) {
      return false
    }
    if (!money.has(cost)) {
      return false
    }
    ns.sleeve.purchaseSleeveAug(index, name)
    money.spend(cost)
    ns.print(`purchase: bought ${name} for sleeve ${index}`)
  }, true)
}
