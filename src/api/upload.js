import http from 'http'

export function upload(
  filename,
  code,
  { host = 'localhost', port = 9990, path = '/', token }
) {
  const cleanPayload = {
    filename: filename.replace(/[\\|/]+/g, '/'),
    code: Buffer.from(code).toString('base64'),
  }
  if (/\//.test(cleanPayload.filename)) {
    cleanPayload.filename = `/${cleanPayload.filename}`
  }

  const stringPayload = JSON.stringify(cleanPayload)
  const options = {
    hostname: host,
    port: port,
    path: path,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': stringPayload.length,
      Authorization: `Bearer ${token}`,
    },
  }

  const request = http.request(options, (response) => {
    response.on(`data`, (d) => {
      const responseBody = Buffer.from(d).toString()

      switch (response.statusCode) {
        case 200:
          console.log(`${filename} has been uploaded!`)
          break
        case 401:
          console.log(
            `ERROR: Failed to push ${filename} to the game!\n${responseBody}`
          )
          break
        default:
          console.log(
            `ERROR: File failed to push, statusCode: ${response.statusCode} | message: ${responseBody}`
          )
          break
      }
    })
  })

  request.write(stringPayload)
  request.end()
}
