#!/usr/bin/env node

import { upload } from './upload.js'
import { promises as fs, existsSync } from 'fs'
import path from 'path'

const FILE_EXTENSIONS = ['.js']

async function main() {
  if (process.argv.length !== 4) {
    console.log(`${process.argv[1]} folder token`)
    process.exit(1)
  }
  const [_node, _command, folder, token] = process.argv

  if (!existsSync(folder)) {
    console.log(`folder ${folder} does not exist`)
    process.exit(1)
  }

  const prefixLength = path.resolve(folder).length + 1
  const files = (await walk(`${folder}/scripts/`))
    .filter((file) => FILE_EXTENSIONS.includes(path.extname(file)))
    .map((file) => {
      const filename = file.substring(prefixLength)
      return { absolutePath: file, filename }
    })

  for (const { absolutePath, filename } of files) {
    const content = await fs.readFile(absolutePath)
    upload(filename, content, { token })
  }
}

async function walk(folder) {
  const entries = await fs.readdir(folder)
  let files = []
  for (const entry of entries) {
    const fullpath = path.resolve(folder, entry)
    const info = await fs.stat(fullpath)
    if (info.isDirectory()) {
      files = [...files, ...(await walk(fullpath))]
    } else {
      files = [...files, fullpath]
    }
  }
  return files
}

main()
